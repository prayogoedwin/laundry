# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Database: database_laundry
# Generation Time: 2020-01-11 19:04:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table barangg
# ------------------------------------------------------------

DROP TABLE IF EXISTS `barangg`;

CREATE TABLE `barangg` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `stok` int(11) NOT NULL,
  `tgl_update` date NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `barangg` WRITE;
/*!40000 ALTER TABLE `barangg` DISABLE KEYS */;

INSERT INTO `barangg` (`id`, `nama`, `stok`, `tgl_update`, `supplier`, `harga`)
VALUES
	(1,'Deterjen Bubuk',2,'2016-02-23','Erwin',8000);

/*!40000 ALTER TABLE `barangg` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jenis
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis`;

CREATE TABLE `jenis` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `jenis` WRITE;
/*!40000 ALTER TABLE `jenis` DISABLE KEYS */;

INSERT INTO `jenis` (`id`, `jenis`, `harga`)
VALUES
	(1,'Paket 1',10000),
	(2,'Paket 2',5000);

/*!40000 ALTER TABLE `jenis` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table konsumen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `konsumen`;

CREATE TABLE `konsumen` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telp` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `konsumen` WRITE;
/*!40000 ALTER TABLE `konsumen` DISABLE KEYS */;

INSERT INTO `konsumen` (`id`, `nama`, `alamat`, `telp`)
VALUES
	(1,'Irwansyah','Jl. Wild West No.12','082445129182'),
	(2,'Edwin Ocky Prayogo','123','081');

/*!40000 ALTER TABLE `konsumen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pemakaian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pemakaian`;

CREATE TABLE `pemakaian` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tgl_pakai` date NOT NULL,
  `barang` varchar(100) NOT NULL,
  `jumlah` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pemakaian` WRITE;
/*!40000 ALTER TABLE `pemakaian` DISABLE KEYS */;

INSERT INTO `pemakaian` (`id`, `tgl_pakai`, `barang`, `jumlah`)
VALUES
	(1,'2016-02-23','Deterjen Bubuk',10);

/*!40000 ALTER TABLE `pemakaian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pembelian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `no` int(10) NOT NULL AUTO_INCREMENT,
  `tgl` date NOT NULL,
  `totali` int(100) NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `barang` varchar(100) NOT NULL,
  `totalh` int(100) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pembelian` WRITE;
/*!40000 ALTER TABLE `pembelian` DISABLE KEYS */;

INSERT INTO `pembelian` (`no`, `tgl`, `totali`, `supplier`, `barang`, `totalh`)
VALUES
	(1,'2016-02-23',10,'Erwin','Deterjen Bubuk',80000),
	(2,'2016-02-23',2,'Erwin','Deterjen Bubuk',16000);

/*!40000 ALTER TABLE `pembelian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pengguna
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('Administrator','Karyawan','Konsumen') NOT NULL,
  `nik` varchar(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `gender` enum('Laki laki','Perempuan') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pengguna` WRITE;
/*!40000 ALTER TABLE `pengguna` DISABLE KEYS */;

INSERT INTO `pengguna` (`id`, `nama`, `username`, `password`, `level`, `nik`, `alamat`, `telp`, `gender`)
VALUES
	(1,'Khalid Insan Tauhid','khalid','827ccb0eea8a706c4c34a16891f84e7b','Administrator','123456789','Sumedang','085221445987','Laki laki'),
	(5,'Ahmad Gunawan','ahmad','827ccb0eea8a706c4c34a16891f84e7b','Karyawan','0153698784','Jl. Angkor Wat No.21','082115221723','Laki laki');

/*!40000 ALTER TABLE `pengguna` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table supplier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telp` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;

INSERT INTO `supplier` (`id`, `nama`, `alamat`, `telp`)
VALUES
	(1,'Erwin','Jl. Water Park No.18','08178171123');

/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table transaksi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) NOT NULL,
  `tarif` int(100) NOT NULL,
  `diskon` int(100) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `tgl_ambil` date NOT NULL,
  `berat` int(10) NOT NULL,
  `pengguna` varchar(100) NOT NULL,
  `konsumen` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `transaksi` WRITE;
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;

INSERT INTO `transaksi` (`id`, `jenis`, `tarif`, `diskon`, `tgl_transaksi`, `tgl_ambil`, `berat`, `pengguna`, `konsumen`)
VALUES
	(1,'Paket 1',100000,0,'2016-02-23','2016-03-01',10,'khalid','Irwansyah'),
	(2,'Paket 2',50000,0,'2016-02-23','2016-03-01',10,'khalid','Irwansyah'),
	(3,'Paket 1',180000,0,'2016-02-23','2016-03-01',20,'khalid','Irwansyah'),
	(4,'Paket 2',90000,0,'2016-02-23','2016-03-01',20,'khalid','Irwansyah'),
	(5,'Paket 2',270000,0,'2020-01-12','2020-01-14',60,'khalid','Irwansyah'),
	(6,'Paket 1',540000,0,'2020-01-12','2020-01-14',60,'ahmad','Irwansyah');

/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
